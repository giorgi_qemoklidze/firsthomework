package com.example.davaleba1



open class MathOperations(){



    fun usg(inputNumber1: Int, inputNumber2 : Int): Int{
     var number1 :Int = inputNumber1
     var number2 :Int = inputNumber2
    while (number1  != number2){
        if (number1>number2)
            number1 -= number2
        else
            number2 -= number1
    }
     return number1
 }
    fun usj(number1: Int,number2: Int):Int{
        return number1 * number2 / usg(number1,number2)
    }

      fun searchChar(inputString : String): Boolean {
          var found : Boolean = false
          for (char in inputString) {
             if ('$'== char) {
                 found=true
             }
         }
          return found
    }

    fun countToHoundred(num:Int =100,sum :Int =0):Int {

        return if (num != 0) {
            if (num % 2 == 0) {

                countToHoundred(num - 1, num + sum)

            } else {
                countToHoundred(num - 1, sum)
            }
        } else
            return sum


    }

    fun flip(inputNumber : Int): Int{
        var reversed = 0
        var num:Int = inputNumber
        while (num != 0){
            val lastDigit = num % 10
            reversed = reversed * 10 + lastDigit
            num /= 10


        }
        return reversed
    }

    fun isPalindrome(inputString : String):Boolean{
        return inputString==inputString.reversed()


    }




}